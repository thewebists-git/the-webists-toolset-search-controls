<?php

add_shortcode('tws-toolset-search-controls', 'tws_toolset_search_controls');

function tws_toolset_search_controls($args, $content = '') {

  $control = TWS_control::make_new_with_args($args);

  return $control->render();

}

add_shortcode('tws-toolset-search-form', 'tws_toolset_search_form');

function tws_toolset_search_form($args, $content = '') {

  $form = TWS_form::make_new_with_args($args, $content);

  return "FORM--" . $form->render() . "--";

}

add_shortcode('tws-toolset-search-submit', 'tws_toolset_search_submit');

function tws_toolset_search_submit($args, $content = '') {
  return TWS_renderer::render_submit($args);
}