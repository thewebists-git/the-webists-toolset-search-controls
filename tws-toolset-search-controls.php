<?php
/*
Plugin Name: The Webists Toolset Search Controls
Plugin URI: http://www.thewebists.com
Description: Misc. shortcodes for generating Toolset Views compatible search forms and fields
Version: 1.0
Author: Andy Brennenstuhl
Author URI: http://www.thewebists.com
*/

/*  Copyright 2016 The Webists

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

include( 'classes/class-tws-control.php');
include( 'classes/class-tws-form.php');
include( 'classes/class-tws-renderer.php');

include( 'shortcodes/shortcodes.php');


// add_action( "admin_print_scripts-" . $settings_page, array( $this, 'admin_scripts' ) );
// public function admin_scripts() {
//     wp_enqueue_script( 'cac-addon-pro', CAC_PRO_URL . "assets/js/cac-addon-pro.js", array( 'jquery' ), CAC_PRO_VERSION );
// }