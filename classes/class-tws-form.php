<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Main class for generating forms
 *
 * Class TWS_form
 */
class TWS_form {

  /**---------------------------*/
  /** Private properties */

  protected $_default_args = array(
    'form_name'             => null,
    'submit_url'             => null,
    'submit_method'          => 'GET',
  );

  protected $_initialized = null;
  protected $_content = null;

  private function __construct() {
    $this->_initialized = false;
  }

    /**
   * Factory pattern for initialising this class. Use TWS_control::makeNewWithArgs($args);
   * @param  (array) $passed_args Initialisation arguments, generally passed from a shortcode
   * @return (object)             Instantiated and initialised Object of this class
   */
  public static function make_new_with_args($passed_args,$passed_content) {
    $form = new TWS_form();
    $form->init_form($passed_args,$passed_content);
    $form->check_name();
    return $form;
  }

  public function init_form($passed_args,$passed_content) {
    
    // Do bitwise checks for succsefull inits
    $init_status = true;
    $init_status &= $this->set_args($passed_args); //parse the arguments
    $init_status &= $this->set_content($passed_content); //parse the arguments

    // Set true/false value from bitwise operations
    $this->_initialized = $init_status;
  }

  public function check_name() {
    if(is_null($this->_args['form_name'])) {
      $this->_args['form_name'] = "tws-search-form-".uniqid();
    }
    return true;
  }

  public function render() {
    return TWS_renderer::render_form($this);
  }

  public function get_content() {
    return $this->_content;
  }

  public function set_content($passed_content) {
    $this->_content = do_shortcode($passed_content);
    return true;
    //TODO implement return false + error log
  }

  public function get_arg($arg){
    if(isset($this->_args[$arg])) {
      return $this->_args[$arg];
    }
  }

  public function set_args($passed_args) {
    $this->_args = wp_parse_args($passed_args, $this->_default_args);
    return true;
  }

}