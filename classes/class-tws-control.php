<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Main class for generating controls
 *
 * Class TWS_control
 */
class TWS_control {

  /**---------------------------*/
  /** Private properties */

  protected $_default_args = array(
    
    'field_name'             => null,
    'type'                   => 'input',
    'label'                  => '',
    'control_get_field_name' => '',
    'control_class'          => '',
    'control_prefix'         => '',
    'control_suffix'         => '',
    'select_default_value'   => '',
    'select_default_text'    => 'Select One...',
    'select_indent_str'      => null,
    'taxonomy'               => '',
    'taxonomy_depth'         => -1,
    'parent_by_slug'         => null,
    'orderby'                => null, 
    'order'                  => null,
    'hide_empty'             => null,
    'include'                => null,
    'exclude'                => null,
    'exclude_tree'           => null,
    'number'                 => null,
    'offset'                 => null,
    'fields'                 => null,
    'name'                   => null,
    'slug'                   => null,
    'hierarchical'           => true,
    'search'                 => null,
    'name__like'             => null,
    'description__like'      => null,
    'pad_counts'             => null,
    'get'                    => null,
    'child_of'               => null,
    'parent'                 => null,
    'childless'              => null,
    'cache_domain'           => null,
    'update_term_meta_cache' => null,
    'meta_query'             => null
  );

  protected $_args = array(); // Stores main arguments used for configurint class and executing data queries (e.g. get_terms). 

  protected $_initialized = null;

  protected $_control_data = null; // Returned data from queries
  protected $_control_data_type = false; // String describing type of output data in case controls deal differently with different types
  protected $_control_type = ""; // Type of HTML control to render
  protected $_control_available_types = array("input", "select", "checkboxes"); // Use this to verify correct settings

  /** /private properties */
  /**---------------------------*/


  private function __construct() {
    $this->_initialized = false;
  }


  /**
   * Factory pattern for initialising this class. Use TWS_control::makeNewWithArgs($args);
   * @param  (array) $passed_args Initialisation arguments, generally passed from a shortcode
   * @return (object)             Instantiated and initialised Object of this class
   */
  public static function make_new_with_args($passed_args) {
    $control = new TWS_control();
    $control->init_control($passed_args);
    return $control;
  }


  public function init_control($passed_args) {
    
    // Do bitwise checks for succsefull inits
    $init_status = true;
    $init_status &= $this->set_args($passed_args); //parse the arguments
    $init_status &= $this->init_field_name(); //make sure the field name is set.
    $init_status &= $this->init_data(); //see if the arguments match up to a data query and execute
    $init_status &= $this->set_type($passed_args['type']);

    // Set true/false value from bitwise operations
    $this->_initialized = $init_status;
  }

  
  public function render() {
    if($this->_initialized) {

      $output = TWS_renderer::render_control($this);

      return $output;
    }
  }


  /**
   * see if the arguments match up to a data query and execute
   * @return &(array) data for populating control
   */
  public function init_data() {
    // Taxonomy data
    if($this->_args['taxonomy']) {
      // Execute get_terms using cleaned array
      $this->_control_data = get_terms(explode('|', $this->_args['taxonomy']), $this->_clean_args($this->_args) );
      $this->_control_data_type = "taxonomy";
    }
    //Single Field data
    //TODO
    return true;
    //TODO implement return false + error log
  }

  public function init_field_name(){
    if (isset($this->_args['fieldname']) && !empty($this->_args['fieldname'])) {
      return true;
    }
    else {
      //TODO implement error log
      return false;
    }
  }

  /**---------------------------*/
  /** Misc getters/setters */
  public function set_type($type) {
    if (!in_array(trim($type), $this->_control_available_types)) {
      return false;
      //TODO implement error log
    }
    else {
      $this->_control_type = trim($type);
      return true;
    }
  }

  //public setter
  public function get_type() {
    return $this->_control_type;
  }

  public function get_data($parent_id = null) {
    if(isset($this->_control_data) && $this->_control_data_type) {
      return $this->_control_data;
    }
  }

  public function get_arg($arg){
    if(isset($this->_args[$arg])) {
      return $this->_args[$arg];
    }
  }

  public function get_data_subset($parent_id) {
    if(isset($this->_control_data) && $this->_control_data_type) {
      switch ($this->_control_data_type) {
        case 'taxonomy':
          $args = $this->_clean_args($this->_args);
          $args['parent'] = $parent_id;
          return get_terms(explode('|', $this->_args['taxonomy']), $args );
      }
    }
  }

  public function get_data_subset_by_slug($parent_slug) {
    return $this->get_data_subset($this->_get_parent_id_by_slug($parent_slug));
  }

  public function get_data_type() {
    if(is_set($this->_control_data) && $this->_control_data_type) {
      return $this->_control_data_type;
    }
  }

  public function set_args($passed_args) {
    $this->_args = wp_parse_args($passed_args, $this->_default_args);
    $this->_process_parent_by_slug();
    return true;
  }
  /** /Misc getters/setters */
  /**---------------------------*/



  /**---------------------------*/
  /** Private functions */

  /**
   * _process_parent_by_slug: parent_by_slug is a supplementary argument for getting taxonomy "parent ids" using get_term_by slug.
   */
  protected function _process_parent_by_slug() {
    if ($this->_args['parent_by_slug']) {
      $this->_args['parent'] = $this->_get_parent_id_by_slug($this->_args['parent_by_slug']);
    }
  }

  protected function _get_parent_id_by_slug($slug) {
    return TWS_control::get_parent_id_by_slug($slug, $this->_args['taxonomy']);
  }

  /**
   * Prepare an args array with all nulled keys removed - otherwise null key are interpreted by get_terms
   * @return (array) Args array with null value keys removed
   */
  protected function _clean_args($args) {
      foreach($args as $key=>$value) {
        if(is_null($value))
          unset($args[$key]);
      }
      return $args;
  }

  /** /Private functions */
  /**---------------------------*/

  /**---------------------------*/
  /** Public Static helper functions */
  public static function get_parent_id_by_slug($slug, $taxonomy) {
    $term = get_term_by('slug', $slug, $taxonomy);
    return $term->term_id;
  }

  /** /Public Static helper functions */
  /**---------------------------*/


}
