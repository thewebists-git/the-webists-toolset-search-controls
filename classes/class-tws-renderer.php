<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Main class for rendering controls
 * Class TWS_renderer
 */
class TWS_renderer {

  public static function render_control($control) {

    $output = "";

    switch ($type = $control->get_type()) {
      case "input":
        $output = "TWS_renderer: This will be an input control...";
      case "select":
        $output = TWS_renderer::_render_label($control);
        $output = TWS_renderer::_render_select($control);
      break;
      case "checkboxes":
        $output = TWS_renderer::_render_label($control);
        $output = TWS_renderer::_render_checkboxes($control);
      break;      default:
        // This should never happen!
        $output = "Wrong Type Set: " . $type;
    }
    return $output;
  }

  public static function render_form($form) {
    $output = "";
    $output .= TWS_renderer::_render_form_pre($form);
    $output .= TWS_renderer::_render_form_content($form);
    $output .= TWS_renderer::_render_form_post($form);
    return $output;
  }

  public static function render_submit($args) {
    $output = "";
    $output .= TWS_renderer::_render_button_submit($args);
    return $output; 
  }


  ########### Controls ########
  protected static function _render_label($control) {
    $output = "";
    if($label = $control->get_arg('label')) {
      $output .= '<label class="tws-label-' . $control->get_arg('fieldname') . '" for="' . $control->get_arg('fieldname') . '">' . $control->get_arg('label') . '</label>';
    }
  }

  protected static function _render_select($control) {
    $output = $control->get_arg('control_prefix');
    $output .= '<select class="' . $control->get_arg('control_class)') . 'tws-select-'.$control->get_arg('fieldname') . '" name="' . $control->get_arg('fieldname') . '[]">';
    $output .= '<option value="' . $control->get_arg('select_default_value') . '" selected>' . $control->get_arg('select_default_text') . '</option>';
    $output .= TWS_renderer::_render_select_options($control->get_data_subset(0), $control);
    $output .= '</select>';
    $output .= $control->get_arg('control_suffix');
    return $output;
  }

  protected static function _render_select_options($data, $control, $taxonomy_depth=-1, $i=1) {
    $default_indent_str = defined('TWS_SEARCH_SELECT_INDENT_STR') ? TWS_SEARCH_SELECT_INDENT_STR : "&nbsp;&nbsp;";
    $indent_str = is_null($control->get_arg('select_indent_str')) ? $default_indent_str : $control->get_arg('select_indent_str');
    if(!is_null($control->get_arg('taxonomy_depth')))  $taxonomy_depth = $control->get_arg('taxonomy_depth');

    $output = "";
    foreach($data as $cat) {
      $output .= '<option value="' . $cat->slug . '">' . str_repeat($indent_str, $i-1) . $cat->name . '</option>';
      if( $i < $taxonomy_depth && ($sub = $control->get_data_subset_by_slug($cat->slug))) {
        $output .= TWS_renderer::_render_select_options($sub, $control, $taxonomy_depth, $i+1);
      }
    }
    return $output;
  }

  protected static function _render_checkboxes($control) {
    $output = $control->get_arg('control_prefix');
    $output .= '<fieldset>';
    $output .= TWS_renderer::_render_checkboxes_options($control->get_data_subset(0), $control);
    $output .= '</fieldset>';
    $output .= $control->get_arg('control_suffix');
    return $output;
  }

  protected static function _render_checkboxes_options($data, $control, $taxonomy_depth=-1, $i=1) {
    $default_indent_str = defined('TWS_SEARCH_SELECT_INDENT_STR') ? TWS_SEARCH_SELECT_INDENT_STR : "&nbsp;&nbsp;";
    $indent_str = is_null($control->get_arg('select_indent_str')) ? $default_indent_str : $control->get_arg('select_indent_str');
    if(!is_null($control->get_arg('taxonomy_depth')))  $taxonomy_depth = $control->get_arg('taxonomy_depth');

    $output = "";
    foreach($data as $cat) {
      $output .= '<div class="checkbox tier' . $i . '">';
      $output .= '<label><input type="checkbox" name="' . $control->get_arg('fieldname') . '[]" value="' . $cat->slug . '">'.$cat->name.'</label>';
      if( $i < $taxonomy_depth && ($sub = $control->get_data_subset_by_slug($cat->slug))) {
        $output .= TWS_renderer::_render_checkboxes_options($sub, $control, $taxonomy_depth, $i+1);
      }
      $output .= '</div>';
    }
    return $output;
  }
  ########### /Controls #######

  ########### Forms ###########
  
  protected static function _render_form_pre($form){
    $output = "";
    $output .= '<form';
    $output .= ' name="' . $form->get_arg('form_name') . '"';
    $output .= ' method="' . $form->get_arg('submit_method') . '">';
    $output .= $form->get_arg('submit_url') ? ' action="' . $form->get_arg('submit_url') . '">' : '';
    return $output;
  }

  
  protected static function _render_form_content($form){
    $output = '';
    $output .= $form->get_content();
    return $output;
  }

  
  protected static function _render_form_post($form){
    $output = '';
    $output .= '</form>';
    return $output;
  }

  ########### /Forms ##########
  
  ############ Buttons ###########
  
  protected static function _render_button_submit($passed_args){
    $default_args = array(
      'button_class'             => 'btn btn-default btn-search-submit',
      'button_text'             => 'Search'
    );

    $args = wp_parse_args($passed_args, $default_args);

    $output = '';
    $output .= '<button class="' . $args['button_class'] . '" type="submit">' . $args['button_text'] . '</button>';
    return $output;
  }



  ############ /Buttons ##########
}
